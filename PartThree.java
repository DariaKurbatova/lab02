import java.util.Scanner;
public class PartThree{
	public static void main(String[] args){
		Scanner keyboard = new Scanner(System.in);
		//asking the user to enter dimensions
		System.out.println("Enter the length of the side of the square: ");
		int sideLength = keyboard.nextInt();
		System.out.println("Enter the length of the rectangle: ");
		int length = keyboard.nextInt();
		System.out.println("Enter the width of the rectangle: ");
		int width = keyboard.nextInt();
		
		//using methods to calculate areas
		
		int squareArea = AreaComputations.areaSquare(sideLength);
		System.out.println("The area of the square is: "+squareArea);

		AreaComputations rectangle = new AreaComputations();
		int recangleArea = rectangle.areaRectangle(length, width);
		System.out.println("The area of the rectangle is: "+recangleArea);
		
		
	}
}