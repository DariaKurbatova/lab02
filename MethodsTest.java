import java.lang.Math;
public class MethodsTest{
	public static void main(String[] args){
		int x = 10;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50);
		methodTwoInputNoReturn(11, 3.3);
		int z = methodNoInputReturnInt();
		System.out.println("The variable printed next should be 6: "+z);
		double sumSqrt = sumSquareRoot(6, 3);
		System.out.println("The square root of the sum of 6 and 3 is: "+sumSqrt);
		String s1 = "hello";
		String s2 = "goodbye";
		System.out.println("The length of s1 is: "+s1.length());
		System.out.println("The length fo s2 is: "+s2.length());
		//Calling addOne and addTwo methods
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	public static void methodNoInputNoReturn(){
		System.out.println("I'm a method that takes no input and returns nothing");
		int x = 50;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int input){
		System.out.println("Inside the method one input no return: "+input);
	}
	public static void methodTwoInputNoReturn(int a, double b){
		System.out.println("Inside the method two input no return: "+a+" and "+b);
	}
	public static int methodNoInputReturnInt(){
		return 6;
	}
	public static double sumSquareRoot(int a, int b){
		int sum = a+b;
		double result = Math.sqrt(sum);
		return result;
	}
}